Pod::Spec.new do |s|
  s.name         = "TIMSDK"
  s.version      = "0.0.1"
  s.summary      = "A short description of TIMSDK."

  s.description  = <<-DESC "A short description of TIMSDK 23232" 
                    DESC
                   
  s.homepage     = "https://gitlab.com/SevenDay/TIMSDK"
  s.license      = ""

  s.author             = { "zhang.wenhai" => "zhang_mr1989@163.com" }
  
  s.platform     = :ios, "8.0"
  s.source       = { :git => "git@gitlab.com:SevenDay/TIMSDK.git"}
  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"

  # s.public_header_files = "Classes/**/*.h"
  s.vendored_frameworks = "TIMSDK/*.framework"

  s.frameworks = "CoreTelephony", "SystemConfiguration"
  s.libraries = "c++", "z", "sqlite3"

end
